#!/usr/bin/env python

""" Default docstring: I can get a fish for a five cent worm """
__author__ = "DysrythMia"

from sys import exit

def desk():
	print "You are standing infront of a wooden desk. There is a notebook, and a cookbook."
	print "Do you go back or read one of the things on the desk?"

	choice = raw_input("> ")
	if "notebook" in choice:
		notebook()
	elif "cookbook" in choice:
		cookbook()
	else:
		print ("I do not understand. Try again")
		desk()

def readmore():
	print "You are done reading. Would you like to read more of the notebook"
	choice = raw_input("or go back to the desk?")

	if "read" in choice:
		notebook()
	if "back" in choice or "desk" in choice:
		desk()
	else:
		print "Invalid choice use keyword 'read' or 'back'"
		readmore()

def cookbook():	
	print "The cookbook has 1 recipe. Do you want to read it?"

	choice = raw_input("> ")

	if "yes" in choice:
		cooktxt = open(cookbook.txt)
			print "The Cookbook reads:" 
			print cooktxt.read()
			print "You stop reading the cookbook, and are back where you were."
			desk()
	elif "no" in choice:
			print "Okay your choice..."
			desk()
	else:
		print "Not a valid choice pick 'yes' or 'no'"


def notebook():
	choice = raw_input("The notebook has 2 pages which do you want to read?")
	if "1" in choice or "one" in choice:
		note1txt = open(notebook1.txt)
		print "Page one is a map of the forest:"
		print note1txt.read()
		readmore()
	elif "2" in choice or "two" in choice:
		print "Page two is instructions on how to burfle hunt:"
		note2txt = open(notebook2.txt)
		print note2txt.read()
		readmore()

	else:
		print("Try again you must pick page 1, 2, or 3.")
		notebook()


def bearroom():
	print "There is a bear here."
	print "The bear has a bunch of honey."
	print "The fat bear is in front of another door"
	print "How are you going to move the bear?"
	bearmoved = False

	while True:
		choice = raw_input("> ")

		if choice == "take honey":
			dead("The bear looks at you then slaps your face off.")
		elif choice == "taunt bear" and not bearmoved:
			print "The bear has moved from the door. You can go through it now."
			bearmoved = True
		elif choice == "taunt bear" and bearmoved:
			dead("THe bear gets pissed off and chews your leg off.")
		elif choice == "open door" and bearmoved:
			goldroom()
		else:
			print "I got no idea what that means."

def cthulhuroom():
	print "Here you see the great evil Cthulhu."
	print "He, it, whatever stares at you and you go insane."
	print "Do you flee for your life or eat your head?"

	choice = raw_input("> ")

	if "flee" in choice:
		start()
	elif "head" in choice:
		dead("Well that was tasty!")
	else:
		cthulhuroom()

def dead(why):
	print why, "Good job!"
	exit(0)

def start():
	print "You are in a cabin surrounded by an extremely dangerous forest where you can be attacked or eaten at anytime."
	print "The cabin has a door, a desk, and a cabinet."
	print "You are very hungry, you must eat dinner soon or you will die."
	print "What is your first choice?"

	choice = raw_input("> ")

	if "door" in choice:
		door()
	elif "desk" in choice:
		desk()
	elif "cabinet" in choice:
		cabinet()
	else:
		print "I don't understand what you are trying to say"
		print "Try typing a keyword 'door', 'desk', or 'cabinet' and try again"
		start()

start()